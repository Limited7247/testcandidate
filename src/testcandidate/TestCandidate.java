/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testcandidate;

import javax.swing.JOptionPane;
import model.Candidate;

/**
 *
 * @author Limited
 */
public class TestCandidate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Candidate[] C = new Candidate[3];
        
        for (int i = 0; i < C.length; i++) {
            C[i] = new Candidate();
            C[i].ReadDialog();
        }
        
        JOptionPane.showMessageDialog(null, "Các thí sinh có tổng điểm lớn hơn 15", "Thông báo", JOptionPane.INFORMATION_MESSAGE);
        for (int i = 0; i < C.length; i++) {
            if (C[i].TongDiem() > 15) {
                
                C[i].ShowNameTDiem();
            }
            
        }
 
        for (int i = 0; i < C.length-1; i++) {
            for (int j = i+1; j < C.length; j++) {
                if (C[i].TongDiem() < C[j].TongDiem())
                {
                    Candidate Ctemp= new Candidate(C[i]);
                    C[i].Set(C[j]);
                    C[j].Set(Ctemp);
                }
                
            }
            
        }
        System.out.println("");
        
        System.out.println("Các thí sinh sau khi sắp xếp: ");
        for (int i = 0; i < C.length; i++) {
            C[i].ShowNameTDiem();
            
        }
    }
    
}
