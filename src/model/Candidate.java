/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.swing.JOptionPane;

/**
 *
 * @author Limited
 */
public class Candidate {
    private String Ma;
    private String Ten;
    private String Ngaysinh;
    private double dToan;
    private double dVan;
    private double dAnh;

    public Candidate() {
        this.Ma = "";
        this.Ten = "";
        this.Ngaysinh = "";
        this.dToan = 0;
        this.dVan = 0;
        this.dAnh = 0;
    }
    
    public Candidate(String Ma, String Ten, String Ngaysinh, double dToan, double dVan, double dAnh) {
        this.Ma = Ma;
        this.Ten = Ten;
        this.Ngaysinh = Ngaysinh;
        this.dToan = dToan;
        this.dVan = dVan;
        this.dAnh = dAnh;
    }
    
    public Candidate(Candidate candidate) {
        this.Ma = candidate.Ma;
        this.Ten = candidate.Ten;
        this.Ngaysinh = candidate.Ngaysinh;
        this.dToan = candidate.dToan;
        this.dVan = candidate.dVan;
        this.dAnh = candidate.dAnh;
    }
        
    public void Set(Candidate candidate)
    {
        this.Ma = candidate.Ma;
        this.Ten = candidate.Ten;
        this.Ngaysinh = candidate.Ngaysinh;
        this.dToan = candidate.dToan;
        this.dVan = candidate.dVan;
        this.dAnh = candidate.dAnh;
    }
    
    public String ShowName()
    {
        return this.Ten;
    }
    
    public void ShowNameTDiem()
    {
        System.out.println(this.Ten + " " + this.TongDiem());
    }
    
    public void ShowDialog()
    {
        String strInfo = "";
        strInfo += "Mã sinh viên: " + this.Ma + "\n";
        strInfo += "Tên: " + this.Ten + "\n";
        strInfo += "Ngày sinh: " + this.Ngaysinh + "\n";
        strInfo += "Điểm Toán: " + this.dToan + "\n";
        strInfo += "Điểm Văn: " + this.dVan + "\n";
        strInfo += "Điểm Anh: " + this.dAnh + "\n";
        
        JOptionPane.showMessageDialog(null, strInfo, "Thông tin Thí sinh", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void ReadDialog()
    {
        this.Ma = JOptionPane.showInputDialog(null, "Nhập Mã sinh viên", "Nhập thông tin Thí sinh", JOptionPane.QUESTION_MESSAGE);
        this.Ten = JOptionPane.showInputDialog(null, "Nhập Tên Thí sinh", "Nhập thông tin Thí sinh", JOptionPane.QUESTION_MESSAGE);
        this.Ngaysinh = JOptionPane.showInputDialog(null, "Nhập Ngày sinh", "Nhập thông tin Thí sinh", JOptionPane.QUESTION_MESSAGE);
        this.dToan = Double.parseDouble(JOptionPane.showInputDialog(null, "Nhập Điểm Toán", "Nhập thông tin Thí sinh", JOptionPane.QUESTION_MESSAGE));
        this.dVan = Double.parseDouble(JOptionPane.showInputDialog(null, "Nhập Điểm Văn", "Nhập thông tin Thí sinh", JOptionPane.QUESTION_MESSAGE));
        this.dAnh = Double.parseDouble(JOptionPane.showInputDialog(null, "Nhập Điểm Anh", "Nhập thông tin Thí sinh", JOptionPane.QUESTION_MESSAGE));
        
    }
    
    public double TongDiem()
    {
        return this.dToan + this.dVan + this.dVan;
    }
    
}
